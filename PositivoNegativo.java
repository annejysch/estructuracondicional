//lea un número entero y calcule si es positivo o negativo.

import java.util.Scanner;

public class PositivoNegativo {
    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        int numero = 0;

        System.out.println("¿Número par, impar o neutro?");
        System.out.println("\nIngrese numero: ");
        numero = leer.nextInt();
        //comparacion
        if(numero > 0){
            System.out.println("Numero positivo");
        } else if (numero == 0) {
            System.out.println("Numero neutro");
        } else{
            System.out.println("Numero negativo");
        }
    }
}

/*
INICIO
    Si
        Numero 0 < negativo
        Escribir "Numero positivo"
    Si no
        Numero = a 0
        Escribir "Numero neutro"
    Si no
        Numero 0 > positivo
        Escribir "Numero negativo"
FIN
*/
