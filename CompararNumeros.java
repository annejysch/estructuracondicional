import java.util.Scanner;

//lea dos números y comprueba si son iguales.
public class CompararNumeros {
    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        int numero1 = 0, numero2 = 0;

        System.out.println("Comparar numeros");
        System.out.println("\nIngresar primer numero");
        numero1 = leer.nextInt();
        System.out.println("\nIngresar segundo numero");
        numero2 = leer.nextInt();
        //comparacion
        if (numero1 == numero2) {
            System.out.println("Los numeros " + numero1 + " y " + numero2 + " son iguales");
        } else {
            System.out.println("Los numeros " + numero1 + " y " + numero2 + " son distintos");
        }
    }
}

/*
INICIO
  Pedir numero
    Si
        numero1 = numero2
        Escribir "Los numeros son iguales"
  Pedir numero
    Si no
        numero1 != numero2
        Escribir "Numero1 distinto de numero2"
FIN
*/
