import java.util.Scanner;

// lea un número entero y muestre si el número es múltiplo de 10.
public class Multiplo10 {
    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        int numero = 0;

        System.out.println("Multiplo de 10");
        System.out.println("\nIngrese numero: ");
        numero = leer.nextInt();
        //condicional
        if (numero %10==0){
            System.out.println("El numero es multiplo de 10");
        } else {
            System.out.println("El numero no es multiplo de 10");
        }
    }
}

/*
INICIO
    Pedir numero
    Si
        numero / 10
        Escribir "El numero es multiplo de 10"
    Si no
        Escribir "El numero no es multiplo de 10"
FIN
*/
