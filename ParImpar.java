import java.util.Scanner;

// Programa Java que lea un número entero y calcule si es par o impar.
public class ParImpar {
    public static void main(String[] args) {
        Scanner leer = new Scanner(System.in);
        int numero = 0;

        System.out.println("Numero par o impar");
        System.out.println("\nIngrese numero: ");
        numero = leer.nextInt();
        //compracion
        if (numero%2==0){
            System.out.println("El numero es par");
        } else{
            System.out.println("El numero es impar");
        }
    }
}

/*
INICIO
    Pedir numero
    Si
        numero / 2
        Escribir "El numero es par"
    Si no
        Escribir "El numero es impar"
FIN
*/
